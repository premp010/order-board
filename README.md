# live-order-board-rest-app

Exposes Rest API to use position book related operation

Author: Prem Paryani

E-mail: premp010@gmail.com

Main-Class: LiveOrderApp.java

Rest-client used: Postman

Maven command for execution:
mvn clean install
mvn exec:java -Dexec.mainclass-com.silverbars.LiveOrderApp

The following rest-api has been exposed

Create Buy Order:
----------------

@POST
http://localhost:8080/events/order

Sample Request:
{
    "securityId "SEC2"
    accountId": "ACC2",
    "quantity" 50
    "tradeType": "buy"
}

Sample Response{
    "tradeId":2,
    "securityId": "SEC2"
    "accountId "ACC2",
    "quantity" 50
    "tradeType" "buy"
}

Create Sell Order:
-----------------

@POST
http://localhost:8080/events/order

Sample Request:
{
    securityId" "SEC3"
    accountId "ACC3"
    "quantity": 50
    tradeType": "sell"
}

Sample Response:
{
    "tradeId" 3,
    "securityId: "SEC3"
    "accountId ACC3"
    "quantity": 50
    "tradeType": "sell"
}

Cancel Order:
------------

@PUT
http://localhost:8080/events/ftradeIdł/cancel

Sample Response:
{
    "tradeId:2,
    "securityId" "SEC2"
    "accountId" "ACC2"
    "quantity" 50,
    "tradeType" "cancel"
}



