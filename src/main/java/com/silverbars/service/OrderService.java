package com.silverbars.service;

import com.silverbars.dao.OrderDAO;
import com.silverbars.dao.impl.OrderDAOImpl;
import com.silverbars.exception.OrderException;
import com.silverbars.model.Order;
import com.silverbars.model.OrderResponse;
import com.silverbars.validator.OrderValidator;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class OrderService {
    private OrderValidator orderValidator = new OrderValidator();
    private OrderDAO orderDAO = new OrderDAOImpl();

    public Order executeOrder(Order order) throws OrderException {
        orderValidator.validateOrderModel(order);
        final long orderId = orderDAO.registerOrder(order);
        return orderDAO.getDetailsByOrderId(orderId);
    }

    public OrderResponse getOrders() throws OrderException {
        List<Order> totalOrderList = orderDAO.getDetails();

        List<Order> buyOrders = totalOrderList.stream().filter(order -> "buy".equalsIgnoreCase(order.getOrderType())).collect(Collectors.toList());
        List<Order> sellOrders = totalOrderList.stream().filter(order -> "sell".equalsIgnoreCase(order.getOrderType())).collect(Collectors.toList());


        //Below is the part where the buy orders are first collected as a map with Price as the key and then their quantities are summed up.
        //After that they are collected as list.
        List<Order> buyOrdersSummed = buyOrders.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.groupingBy(Order::getPrice),
                        map -> map.entrySet().stream()
                                .map(e -> new Order(
                                        e.getValue().get(0).getUserId(),
                                        e.getValue().stream().mapToLong(Order::getQuantity).sum(),
                                        "BUY" ,
                                        e.getKey()
                                ))
                                .collect(Collectors.toList())
                )
        );

        //The sorting requirement where "for SELL orders the orders with lowest prices are displayed first and Opposite is true for the BUY orders."
        buyOrdersSummed.sort(Comparator.comparingDouble(Order::getPrice).reversed());

        //Below is the part where the sell orders are first collected as a map with Price as the key and then their quantities are summed up.
        //After that they are collected as list.
        List<Order> sellOrdersSummed = sellOrders.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.groupingBy(Order::getPrice),
                        map -> map.entrySet().stream()
                                .map(e -> new Order(
                                        e.getValue().get(0).getUserId(),
                                        e.getValue().stream().mapToLong(Order::getQuantity).sum(),
                                        "SELL" ,
                                        e.getKey()
                                ))
                                .collect(Collectors.toList())
                )
        );

        //The sorting requirement where "for SELL orders the orders with lowest prices are displayed first and Opposite is true for the BUY orders."
        sellOrdersSummed.sort(Comparator.comparingDouble(Order::getPrice));

        return new OrderResponse(buyOrdersSummed, sellOrdersSummed);
    }

    public Order cancel(long orderId) throws OrderException {
        orderValidator.validateOrderId(orderId);
        orderDAO.updateOrderByOrderId(orderId);

        return orderDAO.getDetailsByOrderId(orderId);
    }
}
