package com.silverbars.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties (ignoreUnknown = true)
public class Order {
    @JsonIgnore
    private long orderId;

    @JsonProperty(required = true)
    private String userId;

    @JsonProperty(required = true)
    private long quantity;

    @JsonProperty(required = true)
    private String orderType;

    @JsonProperty(required = true)
    private long price;

    public Order() {
    }

    public Order(long orderId, String userId, long quantity, String orderType, long price) {
        this.orderId = orderId;
        this.userId = userId;
        this.quantity = quantity;
        this.orderType = orderType;
        this.price = price;
    }

    public Order(String userId, long quantity, String orderType, long price) {
        this.userId = userId;
        this.quantity = quantity;
        this.orderType = orderType;
        this.price = price;
    }

    public long getOrderId() {
        return orderId;
    }

    public String getUserId() {
        return userId;
    }

    public long getQuantity() {
        return quantity;
    }

    public String getOrderType() {
        return orderType;
    }

    public long getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId == order.orderId &&
                quantity == order.quantity &&
                price == order.price &&
                Objects.equals(userId, order.userId) &&
                Objects.equals(orderType, order.orderType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, userId, quantity, orderType, price);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", userId='" + userId + '\'' +
                ", quantity=" + quantity +
                ", orderType='" + orderType + '\'' +
                ", price=" + price +
                '}';
    }
}
