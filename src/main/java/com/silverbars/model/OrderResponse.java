package com.silverbars.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OrderResponse {

    @JsonProperty(required = true)
    private List<Order> buyOrders;
    @JsonProperty(required = true)
    private List<Order> sellOrders;

    public OrderResponse() {
    }

    public OrderResponse(List<Order> buyOrders, List<Order> sellOrders) {
        this.buyOrders = buyOrders;
        this.sellOrders = sellOrders;
    }


    public List<Order> getBuyOrders() {
        return buyOrders;
    }

    public List<Order> getSellOrders() {
        return sellOrders;
    }
}
