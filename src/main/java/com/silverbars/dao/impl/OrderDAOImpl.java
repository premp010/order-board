package com.silverbars.dao.impl;

import com.silverbars.dao.OrderDAO;
import com.silverbars.exception.OrderException;
import com.silverbars.model.Order;
import com.silverbars.vendor.H2Database;
import org.apache.commons.dbutils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.silverbars.constants.DbQuery.*;

public class OrderDAOImpl implements OrderDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDAOImpl.class);

    @Override
    public long registerOrder(Order order) throws OrderException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try{
            connection = H2Database.getConnection();
            preparedStatement = connection.prepareStatement(CREATE_ORDER);
            preparedStatement.setString(1, order.getUserId());
            preparedStatement.setLong(2, order.getQuantity());
            preparedStatement.setString(3, order.getOrderType());
            preparedStatement.setLong(4, order.getPrice());
            int affectedRows = preparedStatement.executeUpdate();

            if(affectedRows == 0){
                LOGGER.error("Error executing buy order event. ORder ID cannot be generated for Order [{}]", order);
                throw new OrderException("Error executing order event. Order ID cannot be generated");
            }

            resultSet = preparedStatement.getGeneratedKeys();
            if(resultSet.next()){
                return resultSet.getLong(1);
            } else {
                LOGGER.error("Error executing buy order event. ORder ID cannot be generated for Order [{}]", order);
                throw new OrderException("Error executing order event. Order ID cannot be generated");
            }
        } catch (SQLException e) {
            LOGGER.error("Error executing buy order event for Order [{}]",order);
            throw new OrderException("Error creating order event.", e);
        } finally {
            DbUtils.closeQuietly(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public int updateOrderByOrderId(long orderId) throws OrderException {
        Connection connection = null;
        PreparedStatement lockDbRecordStatement = null;
        PreparedStatement updateStatement = null;
        ResultSet resultSet = null;
        Order order = null;
        int updateCount = -1;

        try{
            connection = H2Database.getConnection();
            connection.setAutoCommit(false);
            lockDbRecordStatement = connection.prepareStatement(LOCK_ORDER_BY_ID);
            lockDbRecordStatement.setLong(1, orderId);
            resultSet = lockDbRecordStatement.getResultSet();

            if(resultSet.next()){
                order = new Order(resultSet.getLong("OrderId"), resultSet.getString("UserId"), resultSet.getLong("Quantity"), resultSet.getString("OrderType"), resultSet.getLong("Price"));
                LOGGER.debug("updating in progress, order to cancel for Order [{}]", order);
            }

            if(Objects.isNull(order)){
                throw new OrderException("Failed to execute order for OrderID: " + orderId);
            }

            updateStatement = connection.prepareStatement(UPDATE_ORDER_TYPE);
            updateStatement.setString(1, "cancel");
            updateStatement.setLong(2, orderId);
            updateCount = updateStatement.executeUpdate();
            connection.commit();

            return updateCount;
        } catch (SQLException e) {
            LOGGER.error("Update failed, rollback initiated for: [{}]",orderId, e);
            try{
                if(Objects.isNull(connection))
                    connection.rollback();
            } catch (SQLException se){
                throw new OrderException("Failed to rollback transaction", se);
            }
        } finally {
            DbUtils.closeQuietly(connection);
            DbUtils.closeQuietly(resultSet);
            DbUtils.closeQuietly(lockDbRecordStatement);
            DbUtils.closeQuietly(updateStatement);
        }

        return updateCount;
    }

    @Override
    public List<Order> getDetails() throws OrderException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Order> orderList = new ArrayList<>();

        try{
            connection = H2Database.getConnection();
            preparedStatement = connection.prepareStatement(GET_ORDERS);
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                Order order = new Order(resultSet.getLong("OrderId"), resultSet.getString("UserId"), resultSet.getLong("Quantity"), resultSet.getString("OrderType"), resultSet.getLong("Price"));
                LOGGER.debug("Order retrieved from DB is [{}]", order);
                orderList.add(order);
            }
            return orderList;
        } catch (SQLException e) {
            throw new OrderException("Error reading data from DB", e);
        } finally {
            DbUtils.closeQuietly(connection, preparedStatement, resultSet);
        }
    }

    @Override
    public Order getDetailsByOrderId(long orderId) throws OrderException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Order order = null;

        try{
            connection = H2Database.getConnection();
            preparedStatement = connection.prepareStatement(GET_ORDER_DETAILS_BY_ID);
            preparedStatement.setLong(1, orderId);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                order = new Order(resultSet.getLong("OrderId"), resultSet.getString("UserId"), resultSet.getLong("Quantity"), resultSet.getString("OrderType"), resultSet.getLong("Price"));
                LOGGER.debug("Order retrieved from DB for ID [{}] is [{}]", orderId, order);
            }
            return order;
        } catch (SQLException e) {
            throw new OrderException("getDetailsByOrderId() Error reading order data", e);
        } finally {
            DbUtils.closeQuietly(connection, preparedStatement, resultSet);
        }
    }
}
