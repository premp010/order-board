package com.silverbars.dao;

import com.silverbars.exception.OrderException;
import com.silverbars.model.Order;

import java.util.List;

public interface OrderDAO {
    long registerOrder(Order order) throws OrderException;

    int updateOrderByOrderId(long orderId)throws OrderException;

    List<Order> getDetails()throws OrderException;

    Order getDetailsByOrderId(long orderId)throws OrderException;
}
