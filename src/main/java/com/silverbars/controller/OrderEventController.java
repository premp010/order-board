package com.silverbars.controller;


import com.silverbars.exception.OrderException;
import com.silverbars.model.Order;
import com.silverbars.model.OrderResponse;
import com.silverbars.service.OrderService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.silverbars.constants.RestApiPathConstants.*;

@Path(ORDER_EVENTS_API)
@Produces(MediaType.APPLICATION_JSON)
public class OrderEventController {

    private OrderService orderService = new OrderService();

    @POST
    @Path(ORDER_CREATE_API)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response executeOrder(Order order) throws OrderException {
        Order createdOrder = orderService.executeOrder(order);

        return Response.status(Response.Status.CREATED)
                .entity(createdOrder)
                .build();
    }

    @PUT
    @Path("/{orderId}/cancel")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response cancel(@PathParam("orderId") long orderId) throws OrderException {
        Order order = orderService.cancel(orderId);

        return Response.status(Response.Status.OK)
                .entity(order)
                .build();
    }

    @GET
    @Path(GET_ORDERS_API)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getOrderDetails() throws OrderException {
        OrderResponse orderResponse = orderService.getOrders();

        return Response.status(Response.Status.OK)
                .entity(orderResponse)
                .build();
    }

}
