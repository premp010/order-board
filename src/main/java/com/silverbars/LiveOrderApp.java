package com.silverbars;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.silverbars.DatabaseFactory.*;
import static com.silverbars.InMemoryServer.*;

/**
 * @author Prem Paryani
 * This application will enable the Live Order system
 */
public class LiveOrderApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(LiveOrderApp.class);

    public static void main(String[] args) throws Exception {
        LOGGER.info("Instantiate and Initialize DB with Sample data");
        buildDbWithSampleData(DatabaseVendorType.H2);
        LOGGER.info("DB initialization complete");
        run();
    }
}
