package com.silverbars.validator;

import com.silverbars.exception.ValidationException;
import com.silverbars.model.Order;

import static com.silverbars.validator.ValidatorUtil.*;

public class OrderValidator {
    public void validateOrderModel(Order order){
        required(order, () -> new ValidationException("Order request body is empty"));
        notBlank(order.getUserId(), () -> new ValidationException("User ID is not provided"));
        requireNumber(order.getQuantity(), () -> new ValidationException("Quantity is not valid or null"));
    }

    public void validateOrderId(long orderId){
        requireNumber(orderId, () -> new ValidationException("OrderId is not valid or null"));
        validateIfOrderIdIsNegativeOrZero(orderId, () -> new ValidationException("OrderId cannot be negative or zero"));
    }
}
