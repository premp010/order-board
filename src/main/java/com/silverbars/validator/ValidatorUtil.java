package com.silverbars.validator;

import java.util.Objects;
import java.util.function.Supplier;

public interface ValidatorUtil {
    static <T> void required (T fields, Supplier<? extends RuntimeException> exceptionProducer){
        if(Objects.isNull(fields)){
            throw exceptionProducer.get();
        }
    }

    static void requireNumber (Number number, Supplier<? extends RuntimeException> exceptionProducer){
        if(Objects.isNull(number)){
            throw exceptionProducer.get();
        }
    }

    static void notBlank (String field, Supplier<? extends RuntimeException> exceptionProducer){
        if(Objects.isNull(field) || field.isEmpty()){
            throw exceptionProducer.get();
        }
    }

    static void validateIfOrderIdIsNegativeOrZero (long orderId, Supplier<? extends RuntimeException> exceptionProducer){
        if(orderId <= 0){
            throw exceptionProducer.get();
        }
    }
}
