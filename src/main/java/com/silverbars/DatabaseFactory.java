package com.silverbars;

import com.silverbars.vendor.H2Database;

public interface DatabaseFactory {
    static void buildDbWithSampleData(DatabaseVendorType dbType){
        switch (dbType){
            case H2:
            default:
                H2Database.instantiateAndInitializeDB();
        }
    }
}
