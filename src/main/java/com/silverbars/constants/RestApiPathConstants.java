package com.silverbars.constants;

public class RestApiPathConstants {

    public RestApiPathConstants() {
    }

    public static final String ORDER_EVENTS_API = "/events";
    public static final String GET_ORDERS_API = "/getOrders";
    public static final String ORDER_CREATE_API = "/order";
}
