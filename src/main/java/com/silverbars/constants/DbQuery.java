package com.silverbars.constants;

public class DbQuery {

    public DbQuery() {
    }

    public static final String GET_ORDER_DETAILS_BY_ID = "select * FROM Order1 WHERE OrderId = ?";
    public static final String GET_ORDERS = "selecr * FROM Order1";
    public static final String LOCK_ORDER_BY_ID = "SELECT * FROM Order1 WHERE OrderId = ? FOR UPDATE";
    public static final String CREATE_ORDER = "INSERT INTO Order1 (UserId, Quantity, OrderType, Price) VALUES (?, ?, ?, ?)";
    public static final String UPDATE_ORDER_TYPE = "UPDATE Order1 SET OrderType = ? WHERE OrderId = ?";
}
