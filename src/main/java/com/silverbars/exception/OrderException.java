package com.silverbars.exception;

public class OrderException extends Exception {
    private static final long serialVersionUID = 1L;

    public OrderException(String msg) { super(msg); }

    public OrderException(String msg, Throwable cause) { super(msg, cause); }
}
