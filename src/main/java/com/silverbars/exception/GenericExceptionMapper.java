package com.silverbars.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericExceptionMapper.class);

    public GenericExceptionMapper() {
    }

    @Override
    public Response toResponse(Throwable throwable) {
        LOGGER.info("Creating response with exception [{}]", throwable);

        ExceptionType exceptionType = ExceptionType.getType(throwable.getClass());

        if(exceptionType == ExceptionType.GENERIC_EXCEPTION){
            throwable.printStackTrace();
        }
        ExceptionResponse exceptionResponse = new ExceptionResponse();
        exceptionResponse.setErrorMessage(throwable.getMessage());
        return Response.status(exceptionType.getStatusCode())
                .type(MediaType.APPLICATION_JSON).entity(exceptionResponse)
                .build();
    }
}
