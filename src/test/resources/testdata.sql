DROP TABLE IF EXISTS Order1;

CREATE TABLE Order1 (OrderId LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
UserId VARCHAR(30),
Quantity DECIMAL(19,4),
OrderType VARCHAR(10),
Price DECIMAL(300,4)
);

INSERT INTO Order1 (OrderId, UserId, Quantity, OrderType, Price) VALUES (1, '123', 3.5, 'BUY', 305);

commit;
