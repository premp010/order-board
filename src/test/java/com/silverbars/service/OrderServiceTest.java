package com.silverbars.service;


import com.silverbars.dao.OrderDAO;
import com.silverbars.exception.OrderException;
import com.silverbars.model.Order;

import com.silverbars.model.OrderResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class OrderServiceTest {

    @Mock
    OrderDAO orderDAO;


    @InjectMocks
    OrderService orderService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getOrders() throws OrderException {
        //The list of orders below are what we have initially as our current orders on live board
        Order order1 = new Order(1, "111", 5, "BUY", 505);
        Order order2 = new Order(2, "113", 1, "BUY", 505);
        Order order3 = new Order(3, "112", 3, "BUY", 500);

        List<Order> orderList = new ArrayList<>(Arrays.asList(order1, order2, order3));

        //The list of orders below are what we expect to have as our orders after fetching them
        Order orderExpected1 = new Order(0, "111", 6, "BUY", 505);
        Order orderExpected2 = new Order(0, "112", 3, "BUY", 500);

        List<Order> expectedOrderList = new ArrayList<>(Arrays.asList(orderExpected1, orderExpected2));

        OrderResponse orderResponse = new OrderResponse(expectedOrderList, null);

        when(orderDAO.getDetails()).thenReturn(orderList);

        // The expected and actual lists will match below as it is however:
        // please note that if you try removing the sorting line number 48 in OrderService.java this test will fail, because then the sorting wont take place
        // and the orders wont match because expected and actual will be sorted differently.
        assertEquals(orderService.getOrders().getBuyOrders(), orderResponse.getBuyOrders());
    }
}